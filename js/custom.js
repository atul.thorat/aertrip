$(function () {

    var progressbar = $("#progressbar"),
            progressLabel = $(".progress-label");

    progressbar.progressbar({
        value: false,
        change: function () {
            progressLabel.text(progressbar.progressbar("value") + "%");
        },
        complete: function () {
            progressLabel.text("Complete!");
        }
    });    

    $("body").on("click", "#submit", function () {
        let cnt = 1;
        let flag = 1;
        let errorTxt = [];

        $(".titleStatus").each(function () {
            $(this).remove();
        });

        $(".txtUrl").each(function () {
            let url = $(this).val();
            let urlId = $(this).attr('id');
            var urlRegex = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
            if (urlRegex.test(url)) {
                cnt++;
            } else {
                flag = 0;
                errorTxt.push(urlId);
            }
        });

        if (flag == 1) {
            let progressIntervals = parseInt(100 / (cnt - 1));
            let progressCnt = 1;
            $("#progressbar").show();
            $("#progressbar").progressbar("enable");
            $(".txtUrl").each(function () {
                let url = $(this).val();
                let urlId = $(this).attr('id');
                $.ajax({
                    url: "getContentUrl.php",
                    data: {url: url},
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        progressCnt++;
                        if (data.status == "success") {
                            $("#" + urlId).after('<span class="titleStatus">' + data.title + '</span>');
                            let progressValue = progressbar.progressbar("value") || 0;
                            progressbar.progressbar("value", progressValue + progressIntervals);
                        } else {
                            $("#" + urlId).after('<span class="titleStatus">Error in fetching Url!</span>');
                        }

                        if (cnt == progressCnt) {
                            setTimeout(function () {
                                progressbar.progressbar({
                                    value: false,
                                    change: function () {
                                        progressLabel.text(progressbar.progressbar("value") + "%");
                                    },
                                    complete: function () {
                                        progressLabel.text("Complete!");
                                    }
                                });
                                $("#progressbar").hide();
                            }, 2000);
                        }
                    }
                });
            });
        } else {
            if (errorTxt.length > 0) {
                errorTxt.forEach(function (item, index) {
                    $("#" + item).next('.errorUrl').show();
                    setTimeout(function () {
                        $(".errorUrl").hide();
                    }, 2000);
                });
            }
        }

    });

    $("#repeater").createRepeater();

    $(".sortable").sortable({
        placeholder: "ui-state-highlight"
    });
    $(".sortable").disableSelection();
});